package ru.fintech.homework.utils

import android.content.Context
import ru.fintech.homework.R

object CommonUtil {

    fun getUrl(symbol: String?, context: Context): String {
        return context.getString(R.string.coin_logo_url, symbol).toLowerCase()
    }
}