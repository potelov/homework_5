package ru.fintech.homework.utils

import java.text.DateFormat
import java.util.*

object DateUtil {

    fun getDate(time: Long): String {
        return DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault())
                .format(Date(time * 1000))
    }
}