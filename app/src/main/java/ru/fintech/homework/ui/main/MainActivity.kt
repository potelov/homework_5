package ru.fintech.homework.ui.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import ru.fintech.homework.App
import ru.fintech.homework.R
import ru.fintech.homework.data.model.CoinResponse
import ru.fintech.homework.ui.ScreenState
import ru.fintech.homework.ui.ViewModelFactory
import ru.fintech.homework.ui.detail.DetailActivity
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState != null) {
            tv_welcome.visibility = View.GONE
        }
        App.appComponent.inject(this)
        setupFab()
        setupRecycler()
        setUp()
    }

    private fun setUp() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        viewModel.screenState.observe(this, Observer<ScreenState> { screenState ->
            when (screenState) {
                is ScreenState.Loading -> {
                    tv_error.visibility = View.GONE
                    progress_bar.visibility = View.VISIBLE
                }
                is ScreenState.Error -> {
                    progress_bar.visibility = View.GONE
                    tv_error.visibility = View.VISIBLE
                }
                is ScreenState.MainResponse -> {
                    progress_bar.visibility = View.GONE
                    tv_error.visibility = View.GONE
                    recycler.adapter = MainAdapter(screenState.coinResponse, itemListener)
                }
                else -> {
                    progress_bar.visibility = View.GONE
                    tv_error.visibility = View.GONE
                }
            }
        })
    }

    private fun setupFab() {
        findViewById<FloatingActionButton>(R.id.fab_refresh).apply {
            setOnClickListener {
                tv_welcome.visibility = View.GONE
                viewModel.getCoins()
            }
        }
    }

    private fun setupRecycler() {
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0 && !fab_refresh.isShown) {
                    fab_refresh.show()
                } else if (dy > 0 && fab_refresh.isShown) {
                    fab_refresh.hide()
                }
            }
        })
    }

    private fun openDetailActivity(coinResponse: CoinResponse) {
        val intent = DetailActivity.getStartIntent(this, coinResponse)
        startActivity(intent)
    }

    private var itemListener: ItemClickListener = object : ItemClickListener {
        override fun onItemClicked(coin: CoinResponse) {
            openDetailActivity(coin)
        }
    }
}