package ru.fintech.homework.ui

import ru.fintech.homework.data.model.CoinResponse

sealed class ScreenState {
    object Error : ScreenState()
    object Loading : ScreenState()
    data class MainResponse(val coinResponse: List<CoinResponse>) : ScreenState()
}

