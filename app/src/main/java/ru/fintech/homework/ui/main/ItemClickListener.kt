package ru.fintech.homework.ui.main

import ru.fintech.homework.data.model.CoinResponse

interface ItemClickListener {

    fun onItemClicked(coin: CoinResponse)

}