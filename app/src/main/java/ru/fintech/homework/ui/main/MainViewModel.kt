package ru.fintech.homework.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import ru.fintech.homework.data.Repository
import ru.fintech.homework.ui.ScreenState
import ru.fintech.homework.ui.base.BaseViewModel
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repository: Repository) : BaseViewModel() {

    private val _screenState: MutableLiveData<ScreenState> = MutableLiveData()
    val screenState: LiveData<ScreenState>
        get() = _screenState

    fun getCoins() {
        _screenState.value = ScreenState.Loading
        compositeDisposable.add(repository.getCoinsApiCall()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        { data ->
                            _screenState.value = ScreenState.MainResponse(data)
                        },
                        { error ->
                            _screenState.value = ScreenState.Error
                            Timber.e(error)
                        }
                ))
    }
}

