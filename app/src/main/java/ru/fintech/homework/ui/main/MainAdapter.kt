package ru.fintech.homework.ui.main

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_coin.view.*
import ru.fintech.homework.R
import ru.fintech.homework.data.model.CoinResponse
import ru.fintech.homework.utils.CommonUtil

class MainAdapter(
        private val items:List<CoinResponse>,
        private val itemClickListener: ItemClickListener) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_coin, viewGroup, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(items[position])
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(coin: CoinResponse) {
            view.tv_name.text = coin.getName()
            view.tv_price.text = view.context.getString(R.string.price_format, coin.getPriceUsd())
            view.tv_change.text = view.context.getString(R.string.percent_format, coin.getPercentChange7d())

            if (coin.getPercentChange7d() > 0) {
                view.tv_change.setTextColor(ContextCompat.getColor(view.context, R.color.green700))
            } else {
                view.tv_change.setTextColor(ContextCompat.getColor(view.context, R.color.red700))
            }

            Glide.with(view.context)
                    .load(CommonUtil.getUrl(coin.getSymbol(), view.context))
                    .into(view.iv_logo)

            view.setOnClickListener { itemClickListener.onItemClicked(coin) }
        }
    }
}
