package ru.fintech.homework.ui.base

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import ru.fintech.homework.utils.rx.AppSchedulerProvider
import ru.fintech.homework.utils.rx.SchedulerProvider
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class BaseViewModel : ViewModel() {

    val schedulerProvider: SchedulerProvider = AppSchedulerProvider()
    val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun isOffline(throwable: Throwable?): Boolean {
        return (throwable is UnknownHostException
                || throwable is SocketException
                || throwable is SocketTimeoutException)
    }
}