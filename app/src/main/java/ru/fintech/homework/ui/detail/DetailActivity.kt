package ru.fintech.homework.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import ru.fintech.homework.R
import ru.fintech.homework.data.model.CoinResponse
import ru.fintech.homework.utils.CommonUtil
import ru.fintech.homework.utils.DateUtil

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val coin = intent.getSerializableExtra(EXTRA_INFO) as CoinResponse
        setUp(coin)
    }

    private fun setUp(coin: CoinResponse) {
        tv_title.text = coin.getSymbol()
        coin_title.text = coin.getName()

        price_value.text = getString(R.string.price_format, coin.getPriceUsd())
        market_cap_value.text = getString(R.string.price_format, coin.getMarketCapUsd())

        change_value_1h.text = getPercent(coin.getPercentChange1h())
        change_value_7d.text = getPercent(coin.getPercentChange7d())
        change_value_24h.text = getPercent(coin.getPercentChange24h())

        setTextColor(change_value_1h, coin.getPercentChange1h())
        setTextColor(change_value_7d, coin.getPercentChange7d())
        setTextColor(change_value_24h, coin.getPercentChange24h())

        last_update_value.text = DateUtil.getDate(coin.getLastUpdated())

        Glide.with(this)
                .load(CommonUtil.getUrl(coin.getSymbol(), this))
                .into(coin_logo)

        iv_back.setOnClickListener { onBackPressed() }

    }

    private fun getPercent(num: Double): String {
        return getString(R.string.percent_format, num)
    }

    private fun setTextColor(textView: TextView, num: Double) {
        if (num > 0) {
            textView.setTextColor(ContextCompat.getColor(this, R.color.green700))
        } else {
            textView.setTextColor(ContextCompat.getColor(this, R.color.red700))
        }
    }

    companion object {
        private const val EXTRA_INFO = "extra_info"
        fun getStartIntent(context: Context, coin: CoinResponse): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(EXTRA_INFO, coin)
            return intent
        }
    }
}