package ru.fintech.homework.di.component

import dagger.Component
import ru.fintech.homework.di.module.AppModule
import ru.fintech.homework.di.module.RemoteModule
import ru.fintech.homework.di.module.RepositoryModule
import ru.fintech.homework.ui.main.MainActivity
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RemoteModule::class, RepositoryModule::class))
@Singleton
interface AppComponent {

    fun inject(mainActivity: MainActivity)
}