package ru.fintech.homework.di.module

import dagger.Binds
import dagger.Module
import ru.fintech.homework.data.Repository
import ru.fintech.homework.data.RepositoryImpl

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindsCoinRepository(coinRepositoryImpl: RepositoryImpl): Repository

}