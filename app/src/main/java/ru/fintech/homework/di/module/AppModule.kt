package ru.fintech.homework.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.fintech.homework.App
import ru.fintech.homework.data.Repository
import ru.fintech.homework.ui.ViewModelFactory
import javax.inject.Singleton

@Module
class AppModule(private val application: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideViewModelFactory(coinRepository: Repository): ViewModelFactory {
        return ViewModelFactory(coinRepository)
    }
}