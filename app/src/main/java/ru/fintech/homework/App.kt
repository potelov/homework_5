package ru.fintech.homework

import android.app.Application
import ru.fintech.homework.di.component.AppComponent
import ru.fintech.homework.di.component.DaggerAppComponent
import ru.fintech.homework.di.module.AppModule
import ru.fintech.homework.di.module.RemoteModule

import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .remoteModule(RemoteModule()).build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}
