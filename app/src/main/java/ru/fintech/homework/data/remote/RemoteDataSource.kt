package ru.fintech.homework.data.remote

import io.reactivex.Single
import ru.fintech.homework.data.model.CoinResponse
import javax.inject.Inject


class RemoteDataSource @Inject constructor(private val remoteService: RemoteService) {

    fun requestCoins(): Single<List<CoinResponse>> = remoteService.getCoinsApiCall()
}