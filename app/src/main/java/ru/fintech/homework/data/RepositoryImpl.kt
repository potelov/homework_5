package ru.fintech.homework.data

import io.reactivex.Single
import ru.fintech.homework.data.model.CoinResponse
import ru.fintech.homework.data.remote.RemoteDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepositoryImpl @Inject constructor(private val remoteDataSource: RemoteDataSource) : Repository {

    override fun getCoinsApiCall(): Single<List<CoinResponse>> {
        return remoteDataSource.requestCoins()
    }
}