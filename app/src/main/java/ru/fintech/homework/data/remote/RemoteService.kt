package ru.fintech.homework.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import ru.fintech.homework.data.model.CoinResponse

interface RemoteService {

    @GET("ticker/?limit=20")
    fun getCoinsApiCall(): Single<List<CoinResponse>>
}